<?php

namespace Robusta\Task\Setup;


use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Config;
use Magento\Customer\Model\Customer;

class InstallData implements InstallDataInterface {

    private $eavSetupFactory;
    private $eavConfig;
    public function __construct(EavSetupFactory $eavSetupFactory,Config $eavConfig) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig       = $eavConfig;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
                \Magento\Customer\Model\Customer::ENTITY,
                'profile_image',
                [
                    'type' => 'varchar',
                    'label' => 'Profile Image',
                    'input' => 'image',
                    'required' => false,
                    'visible' => true,
                    'user_defined' => true,
                    'position' => 100,
                    'system' => 0,
                ]
        );
        $attribute = $this->eavConfig->getAttribute(Customer::ENTITY, 'profile_image');
        $attribute->setData(
                'used_in_forms',
                ['adminhtml_customer', 'customer_account_create','customer_account_edit']
        );
        $attribute->save();
    }

   

}
